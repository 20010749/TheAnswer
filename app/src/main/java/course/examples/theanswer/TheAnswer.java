package course.examples.theanswer;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class TheAnswer extends Activity {

	public static final int[] answers = { -20, -10, 0, 100, 1000, 42 };
	public static final int answer = 42;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		// Required call through to Activity.onCreate()
		// Restore any saved instance state
		super.onCreate(savedInstanceState);

		// Set up the application's user interface (content view)
		setContentView(R.layout.answer_layout);

		// Get a reference to a TextView in the content view
		TextView answerView = (TextView) findViewById(R.id.answer_view);

		int val = findAnswer();
		String output = (val == answer) ? "42" : getString(R.string.dontKnow);

		// Set desired text in answerView TextView
        String dr = getString(R.string.question).concat(output);
		answerView.setText(dr);
	}

	private int findAnswer() {
		for (int val : answers) {
			if (val == answer)
				return val;
		}
		return -1;
	}
}
